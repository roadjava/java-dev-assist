# java-dev-assist

#### 介绍
java程序员必备开发助手，前端使用vue2和vue-resource完成ajax方式和java后端数据的交互,借助ace.js这个js插件完成在线代码编辑的实时高亮,由druid来解析sql语法,并经freemarker来渲染,最后由servlet以json格式返回响应。综合这些技术打造出java开发助手项目。
功能:
逆向工程(实现sql转DO对象)

#### 界面-最终效果
![jiemian](./src/main/webapp/resources/doc/jiemian.png)