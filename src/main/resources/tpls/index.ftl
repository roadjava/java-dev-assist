<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="/resources/vue/vue.js"></script>
    <script src="/resources/vue/vue-resource.js"></script>

    <script src="/resources/ace/ace.js" type="text/javascript"></script>
    <script src="/resources/ace/ext-language_tools.js" type="text/javascript"></script>
    <title>java开发助手</title>
    <style>
        #app {
            display: flex;
            justify-content: space-between;
        }
        .ace-editor {
            width: 45vw;
            height: 80vh;
            font-size: 20px;
            margin: 8px 0;
        }
        .btn {
            border-radius: 5px;
            width: 120px;
            height: 40px;
            background-color: #123456;
            border: none;
            color: white;
            padding: 3px;
            outline: none;
            cursor: pointer;
            font-size: 16px;
        }
    </style>
</head>
<body>

<div id="app">
    <div class="ace-container">
        <span>输入mysql建表语句</span>
        <div class="ace-editor" ref="aceSql"></div>
        <div style="text-align:right">
            <input type="button" class="btn" value="实体DO" @click="generateDO"/>
        </div>
    </div>
    <div class="ace-container">
        <span>生成的实体DO</span>
        <div class="ace-editor" ref="aceJava"></div>
    </div>
</div>

<script>
    new Vue({
        el:"#app",
        data:{
            code:"",
            sqlEditor:null,
            resultEditor:null,
            themePath:'ace/theme/monokai',
            sqlModePath:'ace/mode/sql',
            javaModePath:'ace/mode/java',
            jsonModePath:'ace/mode/json'
        },
        methods:{
            generateDO:function() {
                let body ={
                    "cont":this.code,
                    "flag":"generateDO"
                };
               // vue-resource默认的content-type是application/json，配置
               // emulateJson:true改为Content-Type: application/x-www-form-urlencoded
               this.$http.post("/assist",body,{
                   // "emulateJSON":true
               }).then(res => {
                   if(res.data.success) {
                       this.resultEditor.getSession().setMode(this.javaModePath);
                     this.resultEditor.setValue(res.data.data);
                   } else {
                       this.resultEditor.getSession().setMode(this.jsonModePath);
                       // 第三个参数用来美化输出
                       this.resultEditor.setValue(JSON.stringify(res.data,null,"\t"));
                   }
               },err => {
                   this.resultEditor.getSession().setMode(this.jsonModePath);
                   this.resultEditor.setValue(JSON.stringify(err.data));
               });
            }
        },
        mounted:function(){
            this.sqlEditor = ace.edit(this.$refs.aceSql,{
                theme:this.themePath,
                mode:this.sqlModePath,
                enableBasicAutocompletion: true,
                enableSnippets: true, // 代码块，如ife，出来if.. else...
                enableLiveAutocompletion: true// 设置自动提示
            });
            // 监听输入框的变化
            this.sqlEditor.getSession().on('change',e => {
               this.code = this.sqlEditor.getValue();
            });
            this.resultEditor = ace.edit(this.$refs.aceJava,{
                theme:this.themePath,
                mode:this.javaModePath,//设置默认语言
                enableBasicAutocompletion: true,
                enableSnippets: true, // 代码块，如ife，出来if.. else...
                enableLiveAutocompletion: true// 设置自动提示
            });
        }
    });
</script>
</body>
</html>