/**
 * @author zhaodaowen
 * @see <a href="http://www.roadjava.com">乐之者java</a>
 */
public class ${className}DO {
    <#list fields as item>
        <#if item.comment??>
        /**
        * ${item.comment}
        **/
        </#if>
        private ${item.type} ${item.name?uncap_first};
    </#list>

    <#list fields as item>
        public ${item.type} get${item.name}() {
            return this.${item.name?uncap_first};
        }
        public void set${item.name}(${item.type} ${item.name?uncap_first}) {
            this.${item.name?uncap_first} = ${item.name?uncap_first};
        }
    </#list>
}